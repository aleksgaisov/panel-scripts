# Panel scripts

These are bash scripts for displaying useful stats of your GNU/Linux system.
One could use them within their panel in a desktop environment (tested on XFCE and KDE), or a standalone window manager.

Featured scripts:
* *batteryinfo* - battery charge
* *cputemp* - CPU temperature
* *cpuusage* - CPU load
* *diskinfo* - used space on hard drive
* *raminfo* - RAM load
* *upgradable* - upgradable packages (apt package manager)
* *weatherinfo* - temperature and precipitation